# Graph of words

Create a graph of word representation from a text.

Example:

```python
import graph
graph = GraphOfWords(window_size=2)
graph.build_graph('Roses are red. Violets are blue', remove_stopwords=False)
graph.display_graph()
```
